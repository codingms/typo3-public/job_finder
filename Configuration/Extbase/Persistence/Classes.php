<?php
declare(strict_types=1);

return [
    \CodingMs\JobFinder\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
    ],
];
