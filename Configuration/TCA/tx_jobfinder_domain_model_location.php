<?php

$extKey = 'job_finder';
$table = 'tx_jobfinder_domain_model_location';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'label_alt' => 'city',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,street,postal_code,city,country',
        'iconfile' => 'EXT:job_finder/Resources/Public/Icons/iconmonstr-user-14.svg'
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, street, postal_code, city, region, country, latitude, longitude'],
    ],
    'columns' => [
        'information' =>  \CodingMs\AdditionalTca\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'street' => [
            'exclude' => false,
            'label' => $lll . '.street',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'postal_code' => [
            'exclude' => false,
            'label' => $lll . '.postal_code',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'city' => [
            'exclude' => false,
            'label' => $lll . '.city',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'region' => [
            'exclude' => false,
            'label' => $lll . '.region',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'country' => [
            'exclude' => false,
            'label' => $lll . '.country',
            'description' => $lll . '.country_description',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'latitude' => [
            'exclude' => false,
            'label' => $lll . '.latitude',
            'description' => $lll . '.latitude_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('coordinate'),
        ],
        'longitude' => [
            'exclude' => false,
            'label' => $lll . '.longitude',
            'description' => $lll . '.longitude_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('coordinate'),
        ],
        'job' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
