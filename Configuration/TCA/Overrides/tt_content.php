<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function ($extKey) {
        // Plugins
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'JobFinder',
            'JobFinder',
            'Job-Finder List & Detailview'
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'JobFinder',
            'Teaser',
            'Job-Finder Teaser'
        );
        //
        // Include flex forms
        $flexForms = ['Teaser'];
        foreach ($flexForms as $pluginName) {
            $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($extKey);
            $pluginSignature = strtolower($extensionName) . '_' . strtolower($pluginName);
            $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
            $flexForm = 'FILE:EXT:' . $extKey . '/Configuration/FlexForms/' . $pluginName . '.xml';
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
        }
    },
    'job_finder'
);
