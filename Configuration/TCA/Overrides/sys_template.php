<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

//
// Static TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'job_finder',
    'Configuration/TypoScript',
    'Job-Finder'
);
