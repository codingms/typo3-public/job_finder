<?php

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'job_finder',
    'Configuration/PageTS/LinkHandler/application.typoscript',
    'Job-Finder - LinkHandler application'
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    'job_finder',
    'Configuration/PageTS/LinkHandler/job.typoscript',
    'Job-Finder - LinkHandler job'
);

// Page type for jobs
if ((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        0 => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_label.contains_jobs',
        1 => 'jobs',
        2 => 'apps-pagetree-folder-contains-jobs'
    ];
} else {
    $GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
        'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_label.contains_jobs',
        'value' => 'jobs',
        'icon' => 'apps-pagetree-folder-contains-jobs'
    ];
}
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-jobs'] = 'apps-pagetree-folder-contains-jobs';
