<?php

$extKey = 'job_finder';
$table = 'tx_jobfinder_domain_model_contracttype';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,type',
        'iconfile' => 'EXT:job_finder/Resources/Public/Icons/iconmonstr-user-14.svg'
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, type'],
    ],
    'columns' => [
        'information' =>  \CodingMs\AdditionalTca\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'type' => [
            'exclude' => false,
            'label' => $lll . '.type',
            'description' => $lll . '.type_description',
            'config'  => \CodingMs\JobFinder\Tca\Configuration::get('contractType'),
        ],
        'job' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
