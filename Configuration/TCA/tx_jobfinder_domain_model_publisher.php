<?php

$extKey = 'job_finder';
$table = 'tx_jobfinder_domain_model_publisher';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'label_alt' => 'contact_person',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'title,contact_person,street,postal_code,city,phone,email_contact_person,email_central,www',
        'iconfile' => 'EXT:job_finder/Resources/Public/Icons/iconmonstr-user-14.svg'
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, contact_person, street, postal_code, city, phone, email_contact_person, email_central, www'],
    ],
    'columns' => [
        'information' =>  \CodingMs\AdditionalTca\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'description' => $lll . '.title_description',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'contact_person' => [
            'exclude' => false,
            'label' => $lll . '.contact_person',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'street' => [
            'exclude' => false,
            'label' => $lll . '.street',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'postal_code' => [
            'exclude' => false,
            'label' => $lll . '.postal_code',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'city' => [
            'exclude' => false,
            'label' => $lll . '.city',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'phone' => [
            'exclude' => false,
            'label' => $lll . '.phone',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'email_contact_person' => [
            'exclude' => false,
            'label' => $lll . '.email_contact_person',
            'description' => $lll . '.email_contact_person_description',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'email_central' => [
            'exclude' => false,
            'label' => $lll . '.email_central',
            'description' => $lll . '.email_central_description',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'www' => [
            'exclude' => false,
            'label' => $lll . '.www',
            'description' => $lll . '.www_description',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'job' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
