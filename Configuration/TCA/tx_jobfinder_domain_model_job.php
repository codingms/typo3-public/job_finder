<?php

$extKey = 'job_finder';
$table = 'tx_jobfinder_domain_model_job';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$return = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,publication_date,location,description,base_salary,base_salary_unit,category,employment_type,contract_type,occupation,field_of_activity,publisher',
        'iconfile' => 'EXT:job_finder/Resources/Public/Icons/iconmonstr-user-14.svg'
    ],
    'types' => [
        '1' => ['showitem' => '
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_general') . ',
                --palette--;;title_slug_job_duration,
                sub_title,
                --palette--;;publication_date_valid_through_date,
                --palette--;;base_salary_range_base_salary_base_salary_min_base_salary_max_base_salary_unit,
                --palette--;;remote_possible_remote_location,
                --palette--;;job_immediate_start_direct_apply,
                description,
                additional_information,
                skills,
                qualifications,
                job_benefits,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_relations') . ',
                category,
                employment_type,
                contract_type,
                occupation,
                field_of_activity,
                publisher,
                location,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_seo') . ',
                html_title,
                meta_abstract,
                meta_description,
                meta_keywords,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_links') . ',
                link_xing,
                link_indeed,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_images') . ',
                images,
                other_images,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_files') . ',
                files,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_language') . ',
                sys_language_uid,
                l10n_parent,
                l10n_diffsource,
            --div--;' . \CodingMs\AdditionalTca\Tca\Configuration::label('tab_access') . ',
                hidden,
                starttime,
                endtime
            '],
    ],
    'palettes' => [
        'title_slug_job_duration' => ['showitem' => 'title, slug, --linebreak--, job_duration'],
        'publication_date_valid_through_date' => ['showitem' => 'publication_date, valid_through_date'],
        'base_salary_range_base_salary_base_salary_min_base_salary_max_base_salary_unit' => ['showitem' => 'base_salary_range, base_salary, base_salary_min, base_salary_max, base_salary_unit'],
        'remote_possible_remote_location' => ['showitem' => 'remote_possible, remote_location'],
        'job_immediate_start_direct_apply' => ['showitem' => 'job_start_date, job_immediate_start, direct_apply'],
    ],
    'columns' => [
        'information' =>  \CodingMs\AdditionalTca\Tca\Configuration::full('information', '', $extKey),
        'sys_language_uid'  => \CodingMs\AdditionalTca\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_parent', $table),
        'l10n_diffsource' => \CodingMs\AdditionalTca\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AdditionalTca\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AdditionalTca\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AdditionalTca\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AdditionalTca\Tca\Configuration::full('endtime'),
        'title' => [
            'exclude' => 0,
            'label' => $lll . '.title',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'sub_title' => [
            'exclude' => 0,
            'label' => $lll . '.sub_title',
            'config'  => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'slug' => [
            'exclude' => 0,
            'label' => $lll . '.slug',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('slug', false, false, '', [
                'field' => 'title'
            ]),
        ],
        'job_start_date' => [
            'exclude' => false,
            'label' => $lll . '.job_start_date',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get(
                'date',
                false,
                false,
                '',
                [
                    'dbType' => 'date'
                ]
            ),
        ],
        'publication_date' => [
            'exclude' => false,
            'label' => $lll . '.publication_date',
            'description' => $lll . '.publication_date_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get(
                'date',
                false,
                false,
                '',
                [
                    'dbType' => 'date'
                ]
            ),
        ],
        'valid_through_date' => [
            'exclude' => false,
            'label' => $lll . '.valid_through_date',
            'description' => $lll . '.valid_through_date_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get(
                'date',
                false,
                false,
                '',
                [
                    'dbType' => 'date'
                ]
            ),
        ],
        'location' => [
            'exclude' => false,
            'label' => $lll . '.location',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_location',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_location.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_location.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_location.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'html_title' => [
            'exclude' => 0,
            'label' => $lll . '.html_title',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'meta_abstract' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('textareaSmall'),
        ],
        'meta_description' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('textareaSmall'),
        ],
        'meta_keywords' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'description' => [
            'exclude' => false,
            'label' => $lll . '.description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'additional_information' => [
            'exclude' => false,
            'label' => $lll . '.additional_information',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'skills' => [
            'exclude' => false,
            'label' => $lll . '.skills',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'qualifications' => [
            'exclude' => false,
            'label' => $lll . '.qualifications',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'job_benefits' => [
            'exclude' => false,
            'label' => $lll . '.job_benefits',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled]',
        ],
        'base_salary_range' => [
            'exclude' => false,
            'label' => $lll . '.base_salary_range',
            'onChange' => 'reload',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('checkbox'),
        ],
        'base_salary' => [
            'exclude' => false,
            'label' => $lll . '.base_salary',
            'description' => $lll . '.base_salary_description',
            'displayCond' => 'FIELD:base_salary_range:REQ:false',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'base_salary_min' => [
            'exclude' => false,
            'label' => $lll . '.base_salary_min',
            'displayCond' => 'FIELD:base_salary_range:REQ:true',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'base_salary_max' => [
            'exclude' => false,
            'label' => $lll . '.base_salary_max',
            'displayCond' => 'FIELD:base_salary_range:REQ:true',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'base_salary_unit' => [
            'exclude' => false,
            'label' => $lll . '.base_salary_unit',
            'description' => $lll . '.base_salary_unit_description',
            'config' => \CodingMs\JobFinder\Tca\Configuration::get('baseSalaryUnit'),
        ],
        'category' => [
            'exclude' => false,
            'label' => $lll . '.category',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_category',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_category.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_category.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_category.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'employment_type' => [
            'exclude' => false,
            'label' => $lll . '.employment_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_employmenttype',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_employmenttype.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_employmenttype.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_employmenttype.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'contract_type' => [
            'exclude' => false,
            'label' => $lll . '.contract_type',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_contracttype',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_contracttype.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_contracttype.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_contracttype.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'occupation' => [
            'exclude' => false,
            'label' => $lll . '.occupation',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_occupation',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_occupation.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_occupation.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_occupation.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'field_of_activity' => [
            'exclude' => false,
            'label' => $lll . '.field_of_activity',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_fieldofactivity',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_fieldofactivity.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_fieldofactivity.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_fieldofactivity.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'publisher' => [
            'exclude' => false,
            'label' => $lll . '.publisher',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_jobfinder_domain_model_publisher',
                'foreign_table_where' => 'AND tx_jobfinder_domain_model_publisher.pid=###CURRENT_PID### AND tx_jobfinder_domain_model_publisher.sys_language_uid IN (-1,0) ORDER BY tx_jobfinder_domain_model_publisher.title ASC',
                'foreign_sortby' => 'title',
                'minitems' => 1,
                'maxitems' => 1,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => false,
                    ],
                ],
            ],
        ],
        'job_duration' => [
            'exclude' => false,
            'label' => $lll . '.job_duration',
            'config' => \CodingMs\JobFinder\Tca\Configuration::get('jobDuration'),
        ],
        'images' => [
            'exclude' => false,
            'label' => $lll . '.images',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('images', false, false, '', ['field' => 'images']),
        ],
        'other_images' => [
            'exclude' => false,
            'label' => $lll . '.other_images',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('images', false, false, '', ['field' => 'other_images']),
        ],
        'files' => [
            'exclude' => 0,
            'label' => $lll . '.files',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('files')
        ],
        'remote_possible' => [
            'exclude' => false,
            'label' => $lll . '.remote_possible',
            'onChange' => 'reload',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('checkbox'),
        ],
        'remote_location' => [
            'exclude' => 1,
            'displayCond' => 'FIELD:remote_possible:=:1',
            'label' => $lll . '.remote_location',
            'description' => $lll . '.remote_location_description',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string', true),
        ],
        'job_immediate_start' => [
            'exclude' => false,
            'label' => $lll . '.job_immediate_start',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('checkbox'),
        ],
        'direct_apply' => [
            'exclude' => false,
            'label' => $lll . '.direct_apply',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('checkbox'),
        ],
        'link_xing' => [
            'exclude' => false,
            'label' => $lll . '.link_xing',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
        'link_indeed' => [
            'exclude' => false,
            'label' => $lll . '.link_indeed',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('string'),
        ],
    ],
];

if((int)\TYPO3\CMS\Core\Utility\VersionNumberUtility::getCurrentTypo3Version() < 12) {
    $return['ctrl']['cruser_id'] = 'cruser_id';
}
return $return;
