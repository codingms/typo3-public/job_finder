<?php

$EM_CONF['job_finder'] = [
    'title' => 'Job-Finder',
    'description' => 'Job finder extension including job listing, details, application, microdata and more.',
    'category' => 'plugin',
    'author' => 'Thomas Deuling',
    'author_email' => 'typo3@coding.ms',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '4.3.3',
    'constraints' => [
        'depends' => [
            'php' => '7.4.0-8.3.99',
            'typo3' => '11.5.0-12.4.99',
            'additional_tca' => '1.14.2-1.99.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
