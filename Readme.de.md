# Job-Finder Erweiterung für TYPO3

Job-Finder Erweiterung mit Job-Listen, Details Bewerbungs-Formular.


**Features:**

*   Sitemap.xml für Job-Detailansichten
*   Umkreissuche mit Auto-Completer für Jobs
*   Microdaten für Job-Postings (schema.org)
*   Teaser-Inhaltselement für Startseite oder Seitenleiste
*   Link-Handler für die einfache Verlinkung von Job-Detailseiten oder Bewerbungs-Formularen
*   Konfiguration für lesbare URLs

**Pro-Features:**

*   Enthält ein Backendmodul um Jobs und alle erforderlichen Relationen zu verwalten
*   Anzeige von ähnlichen Jobs in der Detailansicht eines Jobs, basierend auf Umkreissuche
*   Online-Bewerbungsformular mit Uploads zur schnellen Bewerbung
*   Automatische Aktualisierung der valid-through und publication-date Werte in den Microdaten
*   PDF-Generator für automatische Job-Detail PDF

Wenn ein zusätzliches oder individuelles Feature benötigt wird - kontaktiere uns gern!


**Links:**

*   [Job-Finder Dokumentation](https://www.coding.ms/documentation/typo3-job-finder "Job-Finder Dokumentation")
*   [Job-Finder Bug-Tracker](https://gitlab.com/codingms/typo3-public/job_finder/-/issues "Job-Finder Bug-Tracker")
*   [Job-Finder Repository](https://gitlab.com/codingms/typo3-public/job_finder "Job-Finder Repository")
*   [TYPO3 Job-Finder Produktdetails](https://www.coding.ms/de/typo3-extensions/typo3-job-finder "TYPO3 Job-Finder Produktdetails")
*   [TYPO3 Job-Finder Dokumentation](https://www.coding.ms/de/dokumentation/typo3-job-finder "TYPO3 Job-Finder Dokumentation")
*   [Demo: WIT-Group](https://www.wit-group.de/stellenangebote "Demo: WIT-Group")
