# Job-Finder Extension for TYPO3

Job finder extension including job listing, details, application and more.


**Features:**

*   Sitemap.xml for jobs
*   Radial search with auto completer for jobs
*   Microdata for Job postings (schema.org)
*   Teaser content element for startsite or sidebar
*   Link handler for linking on job details or applications
*   Configuration for human readable URLs

**Pro-Features:**

*   Includes a backend module to organise jobs and all required relations
*   Display related Jobs in detail-view of a job, based on radial-search
*   Online application form with uploads for an easily application
*   Automatically increasing of valid through and publication-date in microdata
*   PDF generator for automatic Job detail PDF

If you need some additional or custom feature - get in contact!


**Links:**

*   [Job-Finder Documentation](https://www.coding.ms/documentation/typo3-job-finder "Job-Finder Documentation")
*   [Job-Finder Bug-Tracker](https://gitlab.com/codingms/typo3-public/job_finder/-/issues "Job-Finder Bug-Tracker")
*   [Job-Finder Repository](https://gitlab.com/codingms/typo3-public/job_finder "Job-Finder Repository")
*   [TYPO3 Job-Finder Productdetails](https://www.coding.ms/typo3-extensions/typo3-job-finder "TYPO3 Job-Finder Productdetails")
*   [TYPO3 Isotope Documentation](https://www.coding.ms/documentation/typo3-job-finder "TYPO3 Isotope Documentation")
*   [Demo: WIT-Group](https://www.wit-group.de/stellenangebote "Demo: WIT-Group")
