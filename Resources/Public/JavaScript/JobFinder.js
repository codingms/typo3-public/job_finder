/**
 * Author: Thomas Deuling <typo3@coding.ms>
 */
var JobFinder = {

	jobFilter: null,
	jobListHeader: null,
	jobListContent: null,
	jobSearchWordField: null,
	jobOccupationSelect: null,
	jobLocationField: null,
	jobLocationAutoComplete: null,
	eventType: 'click',

	filter: {
		sortBy: '',
		sortOrder: '',
		offset: 0,
		limit: 0,
		distance: 0,
		latitude: 0.0,
		longitude: 0.0,
		resultCount: 0
	},

	/**
	 * Initialize job finder
	 */
	initialize: function() {
		//
		// Detect event type
		if(jQuery('html.touch').length === 1) {
			this.eventType = 'touchstart';
		}
		//
		// If JavaScript is available, show filter feature
		this.jobFilter = jQuery('.job-filter');
		if(this.jobFilter.length > 0) {
			this.jobFilter.slideDown(1000);
			JobFinder.filter.distance = parseInt(this.jobFilter.data('radial-search-distance'), 10);
			//
			//
			this.jobSearchWordField = jQuery('#job-search-word', this.jobFilter);
			this.jobOccupationSelect = jQuery('#job-occupation', this.jobFilter);
			//
			// Location
			this.jobLocationField = jQuery('#job-location', this.jobFilter);
			if(this.jobLocationField.length === 1) {
				//
				// Create the auto-complete object, restricting the search to geographical location types.
				var options = {
					componentRestrictions: {
						country: "de"
					}
				};
				this.jobLocationAutoComplete = new google.maps.places.Autocomplete(this.jobLocationField[0], options);
				//
				// When the user selects an address from the dropdown, populate the address fields in the form.
				this.jobLocationAutoComplete.addListener('place_changed', function() {
					// Get the place details from the autocomplete object.
					var place = JobFinder.jobLocationAutoComplete.getPlace();
					if(typeof place.geometry !== 'undefined') {
						JobFinder.filter.latitude = place.geometry.location.lat();
						JobFinder.filter.longitude = place.geometry.location.lng();
					}
					else {
						JobFinder.filter.latitude = 0.0;
						JobFinder.filter.longitude = 0.0;
					}
					//
					// Ensure to show first page of result
					JobFinder.filter.offset = 0;
					/**
					 * @todo suche triggers
					 */
					JobFinder.searchByFilter();
				});
				//
				// Don't send form on enter
				this.jobLocationField.on('keydown', function(event) {
					if (event.keyCode === 13) {
						event.preventDefault();
					}
				});
			}
		}
		//
		// Job list sorting
		this.jobListHeader = jQuery('.job-list-header');
		if(this.jobListHeader.length > 0) {
			//
			// Assign default sorting
			JobFinder.filter.sortBy = JobFinder.jobListHeader.data('sort-by');
			JobFinder.filter.sortOrder = JobFinder.jobListHeader.data('sort-order');
			//
			// Process list sorting
			var sortColumns = jQuery('[data-sort]', this.jobListHeader);
			jQuery.each(sortColumns, function() {
				var sortColumn = jQuery(this);
				var sortColumnName = sortColumn.data('sort');
				//
				// Set filter css class on column
				if(sortColumnName === JobFinder.filter.sortBy) {
					sortColumn.addClass(JobFinder.filter.sortOrder);
				}
				//
				// Bind sort events
				sortColumn.on(JobFinder.eventType, function() {
					var sortColumn = jQuery(this);
					var sortColumnName = sortColumn.data('sort');
					if(sortColumn.hasClass('asc')) {
						// Switch sorting from asc to desc
						sortColumn.addClass('desc').removeClass('asc');
						JobFinder.sortList(sortColumnName, 'desc');
					}
					else if(sortColumn.hasClass('desc')) {
						// Switch sorting from desc to asc
						sortColumn.addClass('asc').removeClass('desc');
						JobFinder.sortList(sortColumnName, 'asc');
					}
					else {
						// Switch sorting to another column
						jQuery('div', JobFinder.jobListHeader).removeClass('desc').removeClass('asc');
						sortColumn.addClass('asc');
						JobFinder.sortList(sortColumnName, 'asc');
					}
				});
			});
		}
		//
		// Job list content
		this.jobListContent = jQuery('.job-list-content');
		if(this.jobListContent.length > 0) {
			JobFinder.filter.resultCount = jQuery('.row', this.jobListContent).length;
			//
			// Prepare pagination
			jQuery('.job-list-pagination').show();
			JobFinder.filter.limit = parseInt(jQuery('*[data-items-per-page]').data('items-per-page'), 10);
			JobFinder.searchByFilter();
		}
	},

	/**
	 * Search by filter
	 */
	searchByFilter: function(resetOffset) {
		//
		// Offset needs to be reset?
		if (typeof resetOffset === 'undefined') {
			resetOffset = true;
		}
		if(resetOffset) {
			JobFinder.filter.offset = 0;
		}
		//
		// Process search parameter
		if(JobFinder.jobFilter.length) {
			JobFinder.filter.searchWord = JobFinder.jobSearchWordField.val().toLowerCase();
			if(JobFinder.filter.searchWord === '') {
				JobFinder.filter.searchWord = JobFinder.jobOccupationSelect.val().toLowerCase();
			}
			jQuery.each(jQuery('.row', JobFinder.jobListContent), function() {
				var row = jQuery(this);
				row.hide();
				//
				// Search by word
				var searchWordFound = true;
				var rowSearchWord = row.data('search-word').toLowerCase();
				if(JobFinder.filter.searchWord !== '') {
					searchWordFound = (rowSearchWord.indexOf(JobFinder.filter.searchWord) !== -1);
				}
				//
				// Search by location
				var latitude = parseFloat(row.data('latitude'));
				var longitude = parseFloat(row.data('longitude'));
				var locationFound = true;
				if(JobFinder.filter.longitude > 0 && JobFinder.filter.longitude > 0) {
					locationFound = false;
					var distance = JobFinder.getDistance(JobFinder.filter.latitude, JobFinder.filter.longitude, latitude, longitude);
					if(distance <= JobFinder.filter.distance) {
						locationFound = true;
					}
				}
				else {
					JobFinder.jobLocationField.val('');
				}
				// Show row?!
				if(searchWordFound && locationFound) {
					row.show();
				}
			});
		}
		//
		// Count and show result amount
		var count = jQuery('.row:visible', JobFinder.jobListContent).length;
		if(count === 0) {
			//
			// If there is no result, show all
			jQuery('.row', JobFinder.jobListContent).show();
			count = jQuery('.row:visible', JobFinder.jobListContent).length;
			jQuery('.job-list-message-no-result').slideDown(1000);
		}
		else {
			jQuery('.job-list-message-no-result').slideUp(1000);
		}
		//
		// Insert result amount
		jQuery('.job-list-item-count').html(count);
		if(count === 1) {
			jQuery('.job-list-item-count-singular').show();
			jQuery('.job-list-item-count-plural').hide();
		}
		else {
			jQuery('.job-list-item-count-singular').hide();
			jQuery('.job-list-item-count-plural').show();
		}
		//
		// Refresh pagination
		if(jQuery('.job-list-pagination').length) {
			var rows = jQuery('.row:visible', JobFinder.jobListContent);
			JobFinder.filter.resultCount = rows.length;
			JobFinder.refreshPagination();
			var rowIndex = 1;
			jQuery.each(rows, function() {
				var row = jQuery(this);
				if(rowIndex > JobFinder.filter.offset && rowIndex <= JobFinder.filter.offset+JobFinder.filter.limit) {
					row.show();
				}
				else {
					row.hide();
				}
				rowIndex++;
			});
		}
	},

	/**
	 * Reset search word filter
	 */
	resetSearchWordFilter: function() {
		JobFinder.jobSearchWordField.val('');
		JobFinder.jobOccupationSelect.val('');
		JobFinder.searchByFilter();
	},

	/**
	 * Reset location filter
	 */
	resetLocationFilter: function() {
		JobFinder.jobLocationField.val('');
		JobFinder.filter.latitude = 0.0;
		JobFinder.filter.longitude = 0.0;
		JobFinder.searchByFilter();
	},

	/**
	 * Get distance between geo coordinates
	 * @param latitudeFrom
	 * @param longitudeFrom
	 * @param latitudeTo
	 * @param longitudeTo
	 * @returns {number}
	 */
	getDistance: function(latitudeFrom, longitudeFrom, latitudeTo, longitudeTo) {
		var from = new google.maps.LatLng(latitudeFrom, longitudeFrom);
		var to   = new google.maps.LatLng(latitudeTo, longitudeTo);
		var distanceInMeter = google.maps.geometry.spherical.computeDistanceBetween(from, to);
		return distanceInMeter / 1000;
	},

	/**
	 * Sort list
	 * @param sortBy
	 * @param sortOrder
	 */
	sortList: function(sortBy, sortOrder) {
		JobFinder.filter.sortBy = sortBy;
		JobFinder.filter.sortOrder = sortOrder;
		var jobListContent = jQuery('.job-list-content');
		switch(sortBy) {
			case 'publicationDate':
				if(sortOrder === 'asc') {
					jobListContent.children().sortDomElements(function (a, b) {
						var akey = jQuery(a).data('publication-date');
						var bkey = jQuery(b).data('publication-date');
						if (akey === bkey) return 0;
						if (akey < bkey) return -1;
						if (akey > bkey) return 1;
					});
				}
				else {
					jobListContent.children().sortDomElements(function (a, b) {
						var akey = jQuery(a).data('publication-date');
						var bkey = jQuery(b).data('publication-date');
						if (akey === bkey) return 0;
						if (akey > bkey) return -1;
						if (akey < bkey) return 1;
					});
				}
				break;
			case 'title':
				if(sortOrder === 'asc') {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('title');
						var bkey = jQuery(b).data('title');
						if (akey === bkey) return 0;
						if (akey < bkey) return -1;
						if (akey > bkey) return 1;
					});
				}
				else {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('title');
						var bkey = jQuery(b).data('title');
						if (akey === bkey) return 0;
						if (akey > bkey) return -1;
						if (akey < bkey) return 1;
					});
				}
				break;
			case 'location':
				if(sortOrder === 'asc') {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('location');
						var bkey = jQuery(b).data('location');
						if (akey === bkey) return 0;
						if (akey < bkey) return -1;
						if (akey > bkey) return 1;
					});
				}
				else {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('location');
						var bkey = jQuery(b).data('location');
						if (akey === bkey) return 0;
						if (akey > bkey) return -1;
						if (akey < bkey) return 1;
					});
				}
				break;
			case 'employmentType':
				if(sortOrder === 'asc') {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('employment-type');
						var bkey = jQuery(b).data('employment-type');
						if (akey === bkey) return 0;
						if (akey < bkey) return -1;
						if (akey > bkey) return 1;
					});
				}
				else {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('employment-type');
						var bkey = jQuery(b).data('employment-type');
						if (akey === bkey) return 0;
						if (akey > bkey) return -1;
						if (akey < bkey) return 1;
					});
				}
				break;
			case 'category':
				if(sortOrder === 'asc') {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('category-type');
						var bkey = jQuery(b).data('category-type');
						if (akey === bkey) return 0;
						if (akey < bkey) return -1;
						if (akey > bkey) return 1;
					});
				}
				else {
					jobListContent.children().sortDomElements(function(a,b){
						var akey = jQuery(a).data('category-type');
						var bkey = jQuery(b).data('category-type');
						if (akey === bkey) return 0;
						if (akey > bkey) return -1;
						if (akey < bkey) return 1;
					});
				}
				break;
		}
		// Refresh pagination
		JobFinder.searchByFilter();
	},

	/**
	 * Build pagination
	 */
	refreshPagination: function() {
		jQuery.each(jQuery('.job-list-pagination'), function() {
			var pagination = jQuery(this);
			var count = JobFinder.filter.resultCount;
			var limit = JobFinder.filter.limit;
			var offset = JobFinder.filter.offset;
			var pageCount = Math.ceil(count / limit);
			var currentPageNo = (offset / limit) + 1;
			//
			// Remove all page items
			jQuery('.page-item.clone', pagination).remove();
			//
			// Currently on first page
			var paginationPrevious = jQuery('.page-item.previous');
			if(currentPageNo === 1) {
				paginationPrevious.addClass('disabled');
			}
			else if(paginationPrevious.hasClass('disabled')) {
				paginationPrevious.removeClass('disabled');
			}
			if(!paginationPrevious.hasClass('initialized')) {
				paginationPrevious.addClass('initialized');
				paginationPrevious.on('click', function() {
					var limit = JobFinder.filter.limit;
					var offset = JobFinder.filter.offset;
					// Increase offset
					offset -= limit;
					if(offset >= 0) {
						JobFinder.filter.offset = offset;
						JobFinder.searchByFilter(false);
					}
					return false;
				});
			}
			//
			// Currently on last page
			var paginationNext = jQuery('.page-item.next');
			if(currentPageNo === pageCount) {
				paginationNext.addClass('disabled');
			}
			else if(paginationNext.hasClass('disabled')) {
				paginationNext.removeClass('disabled');
			}
			if(!paginationNext.hasClass('initialized')) {
				paginationNext.addClass('initialized');
				paginationNext.on('click', function() {
					var count = JobFinder.filter.resultCount;
					var limit = JobFinder.filter.limit;
					var offset = JobFinder.filter.offset;
					// Increase offset
					offset += limit;
					if(offset < count) {
						JobFinder.filter.offset = offset;
						JobFinder.searchByFilter(false);
					}
					return false;
				});
			}
			//
			// Build page links
			var pageNo = 1;
			for(var i = 0 ; i<count ; i+=limit) {
				var pageItemTemplate = jQuery('.page-item.template', pagination).clone();
				pageItemTemplate.removeClass('template').addClass('clone').removeAttr('style');
				jQuery('a', pageItemTemplate).attr('data-page-no', pageNo).text(pageNo);
				// is current page
				if(pageNo === currentPageNo) {
					pageItemTemplate.addClass('active');
				}
				jQuery(pageItemTemplate).insertBefore(jQuery('.page-item.template', pagination));
				pageNo++;
			}
			pagination.attr('data-pages', pageNo-1);
			jQuery('.page-item.clone a', pagination).on('click', function() {
				var pageLink = jQuery(this);
				var pageNo = parseInt(pageLink.attr('data-page-no'), 10);
				var limit = JobFinder.filter.limit;
				JobFinder.filter.offset = limit * (pageNo - 1);
				JobFinder.searchByFilter(false);
				return false;
			});
		});
	}


};
jQuery(document).ready(function() {
	JobFinder.initialize();
});

jQuery.fn.sortDomElements = (function() {
	return function(comparator) {
		return Array.prototype.sort.call(this, comparator).each(function() {
			this.parentNode.appendChild(this);
		});
	};
})();
