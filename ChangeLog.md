# Job-Finder for TYPO3 Change-Log

*	[TASK] Migrate TypoScript imports



## 2024-09-09 Release of version 4.3.3

*	[TASK] Provide settings and template for user confirmation of an application
*	[TASK] Add accept attribute on HTML of the application form
*	[TASK] Make email_central mandatory in order to avoid issues on online application



## 2024-08-13 Release of version 4.3.2

*	[BUGFIX] Fix SEO HTML title



## 2024-07-25 Release of version 4.3.1

*	[TASK] Change job-finder page-tree icon-size



## 2024-04-19 Release of version 4.3.0

*	[TASK] Add remote location for job and microdata
*	[TASK] Add a demo link in documentation



## 2023-12-20 Release of version 4.2.0

*	[FEATURE] Integrate a PDF generator for Job-Postings
*	[TASK] Optimize version conditions in PHP code



## 2023-11-21 Release of version 4.1.0

*	[FEATURE] Integrate images, other images and files in detail view template
*	[BUGFIX] Fix usage of job finder service
*	[BUGFIX] Fix teaser template condition issue
*	[TASK] Add pages information on pagination wrappers
*	[BUGFIX] Fix JavaScript list filtering by job.occupation in frontend



## 2023-11-16 Release of version 4.0.6

*	[BUGFIX] Fix valid through date field in TCA



## 2023-11-01 Release of version 4.0.5

*	[TASK] Clean up documentation
*	[BUGFIX] Fix get content object for TYPO3 11



## 2023-10-11  Release of version 4.0.4

*	[BUGFIX] Solve dividing base and pro methods of job-finder service



## 2023-09-08  Release of version 4.0.3

*	[BUGFIX] Fix typo in fluid variable, see #23



## 2023-09-04  Release of version 4.0.2

*	[TASK] Add missing translations, see #9



## 2023-08-16  Release of version 4.0.1

*	[BUGFIX] Update job slug field on change



## 2023-08-11  Release of version 4.0.0

*	[TASK] Add job start date in job posting
*	[TASK] Restrict job realtion records to current page in order to get multisite support
*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 10
*	[BUGFIX] Fix composer.json for packagist
*	[TASK] Migrate ext_tables and ext_localconf
*	[TASK] Migrate CSH to TCA descriptions, see #7



## 2024-04-19 Release of version 3.1.0

*	[TASK] Add remote location for job and microdata
*	[BUGFIX] Fix composer.json for packagist
*	[TASK] Migrate ext_tables and ext_localconf
*	[TASK] Migrate CSH to TCA descriptions, see #7



## 2022-10-24  Release of version 3.0.1

*	[TASK] Add TCA descriptions and documentation meta data



## 2022-10-22  Release of version 3.0.0

*	[TASK] Migration for TYPO3 11 and PHP 8.0



## 2022-10-22  Release of version 2.0.0

*	[BUGFIX] Remove debug statement
*	[FEATURE] Add salary range to jobs
*	[FEATURE] Menu processor
*	[BUGFIX] Fix localization parent in TCA
*	[TASK] Remove objectmanager usage for image-service
*	[FEATURE] Add changes for jobfinder pro
*	[BUGFIX] Remove debug statement
*	[FEATURE] Add salary range to jobs
*	[FEATURE] Add meta tags to show job page
*	[TASK] Adjust documentation headers
*	[BUGFIX] Fix mail attachments
*	[TASK] Add missing translation labels and files
*	[BUGFIX] Change format.raw to format.html in order to fix the link-handler links
*	[FEATURE] Add link handler for job details and application
*	[FEATURE] Add message field in application
*	[FEATURE] Add divers/other as gender selection in application
*	[BUGFIX] Add missing sub title configuration in job TCA
*	[TASK] Insert setting for different detail view page and update slug configuration documentation
*	[TASK] Add documentation configuration
*	[TASK] Remove sitemap controller action
*	[FEATURE] Add other images, files and subtitle in job record
*	[TASK] Add more TypoScript settings for enable/disable features
*	[FEATURE] Add new fields for skills, qualifications, benefits and more
*	[TASK] Integrate additional TCA support
*	[TASK] Remove annotations
*	[BUGFIX] Fix invalid characters in translations files
*	[TASK] Preparations for TYPO3 9 + 10
*	[TASK] Add documentations configuration
*	[TASK] Add xml sitemap documentation
*	[TASK] Add human readable url documentation
*	[TASK] Add slug field to job record
*	[TASK] Remove sitemap controller action
*	[FEATURE] Add other images, files and subtitle in job record
*	[TASK] Add more TypoScript settings for enable/disable features
*	[FEATURE] Add new fields for skills, qualifications, benefits and more
*	[TASK] Integrate additional TCA support
*	[TASK] Remove annotations
*	[BUGFIX] Fix invalid characters in translations files
*	[TASK] Preparations for TYPO3 9 + 10



## 2019-10-13  Release of version 1.0.1

*	[TASK] Add Gitlab-CI configuration.
*	[TASK] Remove DEV identifier.
*	[TASK] Optimize configuration files.



## 2018-04-19  Release of version 1.0.0

*	[BUGFIX] Fixing radial search for suggested jobs.

