<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'JobFinder',
            'JobFinder',
            [\CodingMs\JobFinder\Controller\JobFinderController::class => 'list,show,application'],
            [\CodingMs\JobFinder\Controller\JobFinderController::class => 'application']
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'JobFinder',
            'Teaser',
            [\CodingMs\JobFinder\Controller\JobFinderController::class => 'teaser'],
            [\CodingMs\JobFinder\Controller\JobFinderController::class => '']
        );
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            '@import "EXT:job_finder/Configuration/PageTS/tsconfig.typoscript"'
        );
        //
        // Icons
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        //
        // register svg icons: identifier and filename
        $iconsSvg = [
            'icon-content-plugin-jobfinder-job' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-category' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-employmenttype' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-occupation' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-fieldofactivity' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-contracttype' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-publisher' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'icon-content-plugin-jobfinder-location' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
            'apps-pagetree-folder-contains-jobs' => 'Resources/Public/Icons/iconmonstr-user-14.svg',
        ];
        foreach ($iconsSvg as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:job_finder/' . $path]
            );
        }
    }
);
