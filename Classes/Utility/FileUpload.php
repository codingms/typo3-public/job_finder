<?php

declare(strict_types=1);

namespace CodingMs\JobFinder\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\File\BasicFileUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * FileUpload
 *
 * @package job_finder
 * @subpackage Utility
 */
class FileUpload
{

    /**
     * Contains the last error message key
     * @var string
     */
    protected $errorMessage = '';

    /**
     * Upload file
     *
     * @param array $file Files-Array ($_FILES)
     * @param string $targetPath Path where the file should be saved
     * @param string $allowedExtensions Comma separated list of allowed file extensions (* allows all file extensions)
     * @return mixed false or file.png
     */
    public function uploadFile($file, $targetPath = '', $allowedExtensions = '')
    {
        // Check file extension
        if (empty($file['name']) || !self::checkExtension($file['name'], $allowedExtensions)) {
            $this->errorMessage = 'file_extension_disallowed';
            return false;
        }
        // create new filename and upload it
        $targetPasAbsolute = GeneralUtility::getFileAbsFileName($targetPath);
        /** @var BasicFileUtility $basicFileFunctions */
        $basicFileFunctions = GeneralUtility::makeInstance(BasicFileUtility::class);
        $newFile = $basicFileFunctions->getUniqueName($file['name'], $targetPasAbsolute);
        if (GeneralUtility::upload_copy_move($file['tmp_name'], $newFile)) {
            $fileInfo = pathinfo($newFile);
            return $fileInfo['basename'];
        }
        $this->errorMessage = 'moving_uploaded_file_failed';
        return false;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Check extension of given filename
     *
     * @param string Filename like (upload.png)
     * @param string $allowedExtensions Comma separated list of allowed file extensions (* allows all file extensions)
     * @return bool If Extension is allowed
     */
    public static function checkExtension($filename, $allowedExtensions)
    {
        // Allow all file extensions
        if ($allowedExtensions == '*') {
            return true;
        }
        $fileInfo = pathinfo($filename);
        if (!empty($fileInfo['extension']) && GeneralUtility::inList($allowedExtensions,
                strtolower($fileInfo['extension']))) {
            return true;
        }
        return false;
    }

}
