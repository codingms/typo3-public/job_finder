<?php

declare(strict_types=1);

namespace CodingMs\JobFinder\Domain\Model;

use TYPO3\CMS\Core\Resource\FileRepository;
/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\JobFinder\Domain\Repository\FileReferenceRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;

/**
 * Job
 */
class Job extends AbstractEntity
{
    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $subTitle = '';

    /**
     * @var \DateTime
     */
    protected $jobStartDate = null;

    /**
     * @var \DateTime
     */
    protected $publicationDate = null;

    /**
     * @var \DateTime
     */
    protected $validThroughDate = null;

    /**
     * @var \CodingMs\JobFinder\Domain\Model\Location
     */
    protected $location = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var string
     */
    protected $additionalInformation = '';

    /**
     * @var string
     */
    protected $skills = '';

    /**
     * @var string
     */
    protected $qualifications = '';

    /**
     * @var string
     */
    protected $jobBenefits = '';

    /**
     * @var string
     */
    protected $baseSalary = '';

    /**
     * @var string
     */
    protected $baseSalaryUnit = 0;

    /**
     * @var boolean
     */
    protected $baseSalaryRange = false;

    /**
     * @var string
     */
    protected $baseSalaryMin = '';

    /**
     * @var string
     */
    protected $baseSalaryMax = '';

    /**
     * @var \CodingMs\JobFinder\Domain\Model\Category
     */
    protected $category = null;

    /**
     * @var \CodingMs\JobFinder\Domain\Model\EmploymentType
     */
    protected $employmentType = null;

    /**
     * @var \CodingMs\JobFinder\Domain\Model\ContractType
     */
    protected $contractType = null;

    /**
     * @var \CodingMs\JobFinder\Domain\Model\Occupation
     */
    protected $occupation = null;

    /**
     * @var \CodingMs\JobFinder\Domain\Model\FieldOfActivity
     */
    protected $fieldOfActivity = null;

    /**
     * @var \CodingMs\JobFinder\Domain\Model\Publisher
     */
    protected $publisher = null;

    /**
     * @var string
     */
    protected $jobDuration = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $otherImages = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $files;

    /**
     * @var string
     */
    protected $linkXing = '';

    /**
     * @var string
     */
    protected $linkIndeed = '';

    /**
     * @var boolean
     */
    protected $jobImmediateStart = false;

    /**
     * @var boolean
     */
    protected $remotePossible = false;

    /**
     * @var string
     */
    protected $remoteLocation = '';

    /**
     * @var boolean
     */
    protected $directApply = false;

    /**
     * @var string
     */
    protected $htmlTitle;

    /**
     * @var string
     */
    protected $metaAbstract;

    /**
     * @var string
     */
    protected $metaDescription;

    /**
     * @var string
     */
    protected $metaKeywords;

    /**
     * @return void
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all \TYPO3\CMS\Extbase\Persistence\ObjectStorage properties.
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        /**
         * Do not modify this method!
         * It will be rewritten on each save in the extension builder
         * You may modify the constructor of this class instead
         */
        $this->images = new ObjectStorage();
        $this->otherImages = new ObjectStorage();
        $this->files = new ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getSubTitle(): string
    {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     */
    public function setSubTitle(string $subTitle): void
    {
        $this->subTitle = $subTitle;
    }

    /**
     * Returns the jobStartDate
     *
     * @return \DateTime $jobStartDate
     */
    public function getJobStartDate()
    {
        return $this->jobStartDate;
    }

    /**
     * Sets the jobStartDate
     *
     * @param \DateTime $jobStartDate
     * @return void
     */
    public function setJobStartDate(\DateTime $jobStartDate)
    {
        $this->jobStartDate = $jobStartDate;
    }

    /**
     * Returns the publicationDate
     *
     * @return \DateTime $publicationDate
     */
    public function getPublicationDate()
    {
        return $this->publicationDate;
    }

    /**
     * Sets the publicationDate
     *
     * @param \DateTime $publicationDate
     * @return void
     */
    public function setPublicationDate(\DateTime $publicationDate)
    {
        $this->publicationDate = $publicationDate;
    }

    /**
     * @return \DateTime
     */
    public function getValidThroughDate()
    {
        return $this->validThroughDate;
    }

    /**
     * Add a random amount of days
     * @param int $minDays
     * @param int $maxDays
     * @return \DateTime
     */
    public function addValidThroughDateDays($minDays, $maxDays)
    {
        // Add random day amount
        $randomDays = rand($minDays, $maxDays);
        $randomDaysSeconds = $randomDays * (60 * 60 * 24);
        $validThroughSeconds = $this->validThroughDate->getTimestamp();
        $this->validThroughDate->setTimestamp($validThroughSeconds + $randomDaysSeconds);
        return $this->validThroughDate;
    }

    /**
     * @param \DateTime $validThroughDate
     */
    public function setValidThroughDate($validThroughDate)
    {
        $this->validThroughDate = $validThroughDate;
    }

    /**
     * Returns the location
     *
     * @return \CodingMs\JobFinder\Domain\Model\Location $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param \CodingMs\JobFinder\Domain\Model\Location $location
     * @return void
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Returns the category
     *
     * @return \CodingMs\JobFinder\Domain\Model\Category $category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the category
     *
     * @param \CodingMs\JobFinder\Domain\Model\Category $category
     * @return void
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * @param string $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return string
     */
    public function getSkills(): string
    {
        return $this->skills;
    }

    /**
     * @param string $skills
     */
    public function setSkills(string $skills): void
    {
        $this->skills = $skills;
    }

    /**
     * @return string
     */
    public function getQualifications(): string
    {
        return $this->qualifications;
    }

    /**
     * @param string $qualifications
     */
    public function setQualifications(string $qualifications): void
    {
        $this->qualifications = $qualifications;
    }

    /**
     * @return string
     */
    public function getJobBenefits(): string
    {
        return $this->jobBenefits;
    }

    /**
     * @param string $jobBenefits
     */
    public function setJobBenefits(string $jobBenefits): void
    {
        $this->jobBenefits = $jobBenefits;
    }

    /**
     * Returns the baseSalary
     *
     * @return string baseSalary
     */
    public function getBaseSalary()
    {
        return $this->baseSalary;
    }

    /**
     * Sets the baseSalary
     *
     * @param string $baseSalary
     * @return void
     */
    public function setBaseSalary($baseSalary)
    {
        $this->baseSalary = $baseSalary;
    }

    /**
     * @return bool
     */
    public function isBaseSalaryRange(): bool
    {
        return $this->baseSalaryRange;
    }

    /**
     * @param bool $baseSalaryRange
     */
    public function setBaseSalaryRange(bool $baseSalaryRange): void
    {
        $this->baseSalaryRange = $baseSalaryRange;
    }

    /**
     * @return string
     */
    public function getBaseSalaryMin(): string
    {
        return $this->baseSalaryMin;
    }

    /**
     * @param string $baseSalaryMin
     */
    public function setBaseSalaryMin(string $baseSalaryMin): void
    {
        $this->baseSalaryMin = $baseSalaryMin;
    }

    /**
     * @return string
     */
    public function getBaseSalaryMax(): string
    {
        return $this->baseSalaryMax;
    }

    /**
     * @param string $baseSalaryMax
     */
    public function setBaseSalaryMax(string $baseSalaryMax): void
    {
        $this->baseSalaryMax = $baseSalaryMax;
    }

    /**
     * Returns the employmentType
     *
     * @return \CodingMs\JobFinder\Domain\Model\EmploymentType employmentType
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * Sets the employmentType
     *
     * @param \CodingMs\JobFinder\Domain\Model\EmploymentType $employmentType
     * @return void
     */
    public function setEmploymentType(EmploymentType $employmentType)
    {
        $this->employmentType = $employmentType;
    }

    /**
     * Returns the baseSalaryUnit
     *
     * @return string $baseSalaryUnit
     */
    public function getBaseSalaryUnit()
    {
        return $this->baseSalaryUnit;
    }

    /**
     * Sets the baseSalaryUnit
     *
     * @param string $baseSalaryUnit
     * @return void
     */
    public function setBaseSalaryUnit($baseSalaryUnit)
    {
        $this->baseSalaryUnit = $baseSalaryUnit;
    }

    /**
     * Returns the contractType
     *
     * @return \CodingMs\JobFinder\Domain\Model\ContractType $contractType
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Sets the contractType
     *
     * @param \CodingMs\JobFinder\Domain\Model\ContractType $contractType
     * @return void
     */
    public function setContractType(ContractType $contractType)
    {
        $this->contractType = $contractType;
    }

    /**
     * Returns the occupation
     *
     * @return \CodingMs\JobFinder\Domain\Model\Occupation $occupation
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Sets the occupation
     *
     * @param \CodingMs\JobFinder\Domain\Model\Occupation $occupation
     * @return void
     */
    public function setOccupation(Occupation $occupation)
    {
        $this->occupation = $occupation;
    }

    /**
     * Returns the fieldOfActivity
     *
     * @return \CodingMs\JobFinder\Domain\Model\FieldOfActivity $fieldOfActivity
     */
    public function getFieldOfActivity()
    {
        return $this->fieldOfActivity;
    }

    /**
     * Sets the fieldOfActivity
     *
     * @param \CodingMs\JobFinder\Domain\Model\FieldOfActivity $fieldOfActivity
     * @return void
     */
    public function setFieldOfActivity(FieldOfActivity $fieldOfActivity)
    {
        $this->fieldOfActivity = $fieldOfActivity;
    }

    /**
     * Returns the publisher
     *
     * @return \CodingMs\JobFinder\Domain\Model\Publisher
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Sets the publisher
     *
     * @param \CodingMs\JobFinder\Domain\Model\Publisher
     * @return void
     */
    public function setPublisher(Publisher $publisher)
    {
        $this->publisher = $publisher;
    }

    /**
     * @return string
     */
    public function getJobDuration()
    {
        return $this->jobDuration;
    }

    /**
     * @param string $jobDuration
     */
    public function setJobDuration($jobDuration)
    {
        $this->jobDuration = $jobDuration;
    }

    /**
     * Adds a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function addImage(FileReference $image)
    {
        $this->images->attach($image);
    }

    /**
     * Removes a image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $imageToRemove The image to be removed
     * @return void
     */
    public function removeImage(FileReference $imageToRemove)
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $images
     * @return void
     */
    public function setImages(ObjectStorage $images)
    {
        $this->images = $images;
    }

    /**
     * Adds a otherImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $otherImage
     * @return void
     */
    public function addOtherImage(FileReference $otherImage)
    {
        $this->otherImages->attach($otherImage);
    }

    /**
     * Removes a otherImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $otherImageToRemove The otherImage to be removed
     * @return void
     */
    public function removeOtherImage(FileReference $otherImageToRemove)
    {
        $this->otherImages->detach($otherImageToRemove);
    }

    /**
     * Returns the otherImages
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $otherImages
     */
    public function getOtherImages()
    {
        return $this->otherImages;
    }

    /**
     * Sets the otherImages
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $otherImages
     * @return void
     */
    public function setOtherImages(ObjectStorage $otherImages)
    {
        $this->otherImages = $otherImages;
    }

    /**
     * Adds a file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     * @return void
     */
    public function addFile(FileReference $file)
    {
        $this->files->attach($file);
    }

    /**
     * Removes a file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove The file to be removed
     * @return void
     */
    public function removeFile(FileReference $fileToRemove)
    {
        $this->files->detach($fileToRemove);
    }

    /**
     * Returns the files
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $files
     */
    public function getFiles()
    {
        $files = $this->files;
        if(count($this->files) > 0) {
            if(!$this->getIsNotTranslated()) {
                $files = [];
                /** @var \TYPO3\CMS\Core\Resource\FileRepository $fileRepository */
                $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
                $fileObjects = $fileRepository->findByRelation('tx_jobfinder_domain_model_job', 'files', $this->_localizedUid);
                if(count($fileObjects) > 0) {
                    /** @var FileReference $fileObject */
                    foreach($fileObjects as $fileObject) {
                        $fileReferenceUid = $fileObject->getUid();
                        $fileReferenceRepository = GeneralUtility::makeInstance(FileReferenceRepository::class);
                        $files[] = $fileReferenceRepository->findByIdentifier($fileReferenceUid);
                    }
                }
            }
        }
        return $files;
    }

    /**
     * Identify, if this is a non translated record.
     * This means this record is in default language.
     * @return bool
     */
    public function getIsNotTranslated() {
        return $this->_localizedUid === $this->uid;
    }

    /**
     * Sets the files
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $files
     * @return void
     */
    public function setFiles(ObjectStorage $files)
    {
        $this->files = $files;
    }

    /**
     * @return string
     */
    public function getLinkXing()
    {
        return $this->linkXing;
    }

    /**
     * @param string $linkXing
     */
    public function setLinkXing($linkXing)
    {
        $this->linkXing = $linkXing;
    }

    /**
     * @return string
     */
    public function getLinkIndeed()
    {
        return $this->linkIndeed;
    }

    /**
     * @param string $linkIndeed
     */
    public function setLinkIndeed($linkIndeed)
    {
        $this->linkIndeed = $linkIndeed;
    }

    /**
     * @return bool
     */
    public function isJobImmediateStart(): bool
    {
        return $this->jobImmediateStart;
    }

    /**
     * @param bool $jobImmediateStart
     */
    public function setJobImmediateStart(bool $jobImmediateStart): void
    {
        $this->jobImmediateStart = $jobImmediateStart;
    }

    /**
     * @return bool
     */
    public function isRemotePossible(): bool
    {
        return $this->remotePossible;
    }

    /**
     * @param bool $remotePossible
     */
    public function setRemotePossible(bool $remotePossible): void
    {
        $this->remotePossible = $remotePossible;
    }

    public function getRemoteLocation(): string
    {
        return $this->remoteLocation;
    }

    public function setRemoteLocation(string $remoteLocation): void
    {
        $this->remoteLocation = $remoteLocation;
    }

    /**
     * @return bool
     */
    public function isDirectApply(): bool
    {
        return $this->directApply;
    }

    /**
     * @param bool $directApply
     */
    public function setDirectApply(bool $directApply): void
    {
        $this->directApply = $directApply;
    }

    /**
     * @return string
     */
    public function getHtmlTitle(): string
    {
        return $this->htmlTitle;
    }

    /**
     * @param string $htmlTitle
     */
    public function setHtmlTitle(string $htmlTitle): void
    {
        $this->htmlTitle = $htmlTitle;
    }

    /**
     * @return string
     */
    public function getMetaAbstract(): string
    {
        return $this->metaAbstract;
    }

    /**
     * @param string $metaAbstract
     */
    public function setMetaAbstract(string $metaAbstract): void
    {
        $this->metaAbstract = $metaAbstract;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords(string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

}
