<?php

declare(strict_types=1);

namespace CodingMs\JobFinder\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\JobFinder\Domain\Model\Job;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;

/**
 * Job repository
 *
 * @package job_finder
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class JobRepository extends Repository
{

    /**
     * @param string $sortBy
     * @param string $sortOrder
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllInitialize($sortBy, $sortOrder)
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\QueryInterface $query */
        $query = $this->createQuery();
        if ($sortOrder === 'desc') {
            $query->setOrderings([$sortBy => QueryInterface::ORDER_DESCENDING]);
        } else {
            $query->setOrderings([$sortBy => QueryInterface::ORDER_ASCENDING]);
        }
        return $query->execute();
    }

    /**
     * @param string $sortBy
     * @param string $sortOrder
     * @param int $limit
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllTeaser($sortBy, $sortOrder, $limit = 0)
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\QueryInterface $query */
        $query = $this->createQuery();
        if ($sortOrder === 'desc') {
            $query->setOrderings([$sortBy => QueryInterface::ORDER_DESCENDING]);
        } else {
            $query->setOrderings([$sortBy => QueryInterface::ORDER_ASCENDING]);
        }
        if ($limit > 0) {
            $query->setLimit($limit);
        }
        return $query->execute();
    }

    /**
     * @param Job $job
     * @param array $settings
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findSuggestedJobs($job, $settings)
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\QueryInterface $query */
        $query = $this->createQuery();
        // Build statement
        $sql = 'SELECT ';
        $sql .= 'location.uid, location.latitude, location.longitude, (6371 * acos( cos( radians(' . $job->getLocation()->getLatitude() . ') ) * cos( radians(`location`.`latitude`) ) * ';
        $sql .= 'cos( radians(`location`.`longitude`) - radians(' . $job->getLocation()->getLongitude() . ') ) + sin( radians(' . $job->getLocation()->getLatitude() . ') ) * ';
        $sql .= 'sin( radians(`location`.`latitude`) ) ) ) AS distanceValue, job.* ';
        $sql .= 'FROM tx_jobfinder_domain_model_job as job, ';
        $sql .= 'tx_jobfinder_domain_model_location as location ';
        $sql .= 'WHERE job.location = location.uid AND job.uid != ' . $job->getUid() . ' AND job.deleted=0 AND job.hidden=0 HAVING distanceValue < ' . $settings['distance'] . ' ';
        $sql .= 'LIMIT ' . $settings['limit'] . ' ';
        $query->statement($sql);
        $result = $query->execute();
        return $result;
    }

    /**
     * @param array $filter
     * @param boolean $count
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllForBackendList(array $filter = array(), $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] !== '') {
                if ($filter['sortingOrder'] === 'asc') {
                    $query->setOrderings(array($filter['sortingField'] => QueryInterface::ORDER_ASCENDING));
                } else {
                    if ($filter['sortingOrder'] === 'desc') {
                        $query->setOrderings(array($filter['sortingField'] => QueryInterface::ORDER_DESCENDING));
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        } else {
            return $query->execute()->count();
        }
    }


}
