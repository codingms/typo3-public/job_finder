<?php

declare(strict_types=1);

namespace CodingMs\JobFinder\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AdditionalTca\Tca\Configuration as ConfigurationDefaults;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

class Configuration extends ConfigurationDefaults
{

    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label
     * @param array $options
     * @return array
     */
    public static function get($type, $required = false, $readonly = false, $label = '', array $options=[]): array
    {
        switch ($type) {
            case 'employmentType':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.full_time',
                            'value' => 'FULL_TIME'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.part_time',
                            'value' => 'PART_TIME'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.contractor',
                            'value' => 'CONTRACTOR'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.temporary',
                            'value' => 'TEMPORARY'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.intern',
                            'value' => 'INTERN'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.volunteer',
                            'value' => 'VOLUNTEER'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.per_diem',
                            'value' => 'PER_DIEM'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.other',
                            'value' => 'OTHER'
                        ],
                    ],
                    'size' => 1,
                    'minitems' => 1,
                    'maxitems' => 1,
                    'eval' => ''
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.full_time',
                            'FULL_TIME'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.part_time',
                            'PART_TIME'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.contractor',
                            'CONTRACTOR'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.temporary',
                            'TEMPORARY'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.intern',
                            'INTERN'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.volunteer',
                            'VOLUNTEER'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.per_diem',
                            'PER_DIEM'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_employmenttype.other',
                            'OTHER'
                        ],
                    ];
                }
                break;
            case 'contractType':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.employee',
                            'value' => 'EMPLOYEE'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.student',
                            'value' => 'STUDENT'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.internship',
                            'value' => 'INTERNSHIP'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.apprenticeship',
                            'value' => 'APPRENTICESHIP'
                        ],
                    ],
                    'size' => 1,
                    'minitems' => 1,
                    'maxitems' => 1,
                    'eval' => ''
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.employee',
                            'EMPLOYEE'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.student',
                            'STUDENT'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.internship',
                            'INTERNSHIP'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang_db.xlf:tx_jobfinder_domain_model_contracttype.apprenticeship',
                            'APPRENTICESHIP'
                        ],
                    ];
                }
                break;
            case 'baseSalaryUnit':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.hour',
                            'value' => 'HOUR'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.day',
                            'value' => 'DAY'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.week',
                            'value' => 'WEEK'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.month',
                            'value' => 'MONTH'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.year',
                            'value' => 'YEAR'
                        ],
                    ],
                    'size' => 1,
                    'minitems' => 1,
                    'maxitems' => 1,
                    'eval' => ''
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.hour',
                            'HOUR'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.day',
                            'DAY'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.week',
                            'WEEK'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.month',
                            'MONTH'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.year',
                            'YEAR'
                        ],
                    ];
                }
                break;
            case 'jobDuration':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'items' => [
                        [
                            'label' => '',
                            'value' => ''
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.limited',
                            'value' => 'limited'
                        ],
                        [
                            'label' => 'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.permanent',
                            'value' => 'permanent'
                        ],
                    ],
                    'size' => 1,
                    'minitems' => 1,
                    'maxitems' => 1,
                    'eval' => ''
                ];
                if ((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                    $config['items'] = [
                        [
                            '',
                            ''
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.limited',
                            'limited'
                        ],
                        [
                            'LLL:EXT:job_finder/Resources/Private/Language/locallang.xlf:tx_jobfinder_label.permanent',
                            'permanent'
                        ],
                    ];
                }
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label, $options);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }
}
