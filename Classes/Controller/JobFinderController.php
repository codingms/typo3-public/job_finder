<?php

declare(strict_types=1);

namespace CodingMs\JobFinder\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\JobFinder\Domain\Model\Job;
use CodingMs\JobFinder\Domain\Repository\JobRepository;
use CodingMs\JobFinder\Domain\Repository\OccupationRepository;
use CodingMs\JobFinder\PageTitle\PageTitleProvider;
use CodingMs\JobFinder\Utility\FileUpload;
use CodingMs\JobFinder\Utility\ValidationUtility;
use DateTime;
use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Http\UploadedFile;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerInterface;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\UnknownObjectException;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * JobController
 */
class JobFinderController extends ActionController
{

    /**
     * Data og the content element
     * @var array<mixed>
     */
    protected array $content = [];

    protected JobRepository $jobRepository;
    protected OccupationRepository $occupationRepository;
    protected PersistenceManager $persistenceManager;

    public function __construct(
        JobRepository $jobRepository,
        OccupationRepository $occupationRepository,
        PersistenceManager $persistenceManager
    ) {
        $this->jobRepository = $jobRepository;
        $this->occupationRepository = $occupationRepository;
        $this->persistenceManager = $persistenceManager;
    }

    /**
     * Initialize actions
     */
    public function initializeAction()
    {
        // Get content data
        if((int)VersionNumberUtility::getCurrentTypo3Version() > 11) {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->request->getAttribute('currentContentObject');
        } else {
            /** @var ContentObjectRenderer $contentObject */
            $contentObject = $this->configurationManager->getContentObject();
        }
        $this->content = $contentObject->data;
        //
        // Hide pro features
        if (!ExtensionManagementUtility::isLoaded('job_finder_pro')) {
            $this->settings['detail']['suggestedJobs']['active'] = false;
            $this->settings['application']['active'] = false;
        }
    }

    /**
     * List of jobs
     *
     * @return void
     */
    public function listAction(): ResponseInterface
    {
        $sortBy = $this->settings['list']['defaultSorting']['sortBy'];
        $sortOrder = $this->settings['list']['defaultSorting']['sortOrder'];
        $jobs = $this->jobRepository->findAllInitialize($sortBy, $sortOrder);
        // Don't use repository, because it results entries, that aren't in use.
        //$occupations = $this->occupationRepository->findAllFormSelect();
        $occupations = [];
        if ($jobs->count() === 0) {
            $this->addFlashMessage(
                $this->translate('tx_jobfinder_message.no_jobs_available'),
                $this->translate('tx_jobfinder_message.ok_headline'),
                AbstractMessage::INFO
            );
        } else {
            /** @var Job $job */
            foreach ($jobs as $job) {
                $occupations[$job->getOccupation()->getTitle()] = $job->getOccupation();
            }
        }
        asort($occupations);
        $this->view->assign('jobs', $jobs);
        $this->view->assign('occupations', $occupations);
        $this->view->assign('content', $this->content);
        return $this->htmlResponse();
    }

    /**
     * Display job details
     *
     * @return void
     * @throws NoSuchArgumentException
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     * @throws IllegalObjectTypeException
     * @throws UnknownObjectException
     */
    public function showAction(): ResponseInterface
    {
        /** @var Job $job */
        $job = null;
        $jobUid = 'undefined';
        if ($this->request->hasArgument('job')) {
            $jobUid = (int)$this->request->getArgument('job');
            $job = $this->jobRepository->findByIdentifier($jobUid);
        }
        if (!($job instanceof Job)) {
            // Job not found?!
            // Redirect to list view
            $this->addFlashMessage(
                $this->translate('tx_jobfinder_message.job_not_found', [$jobUid]),
                $this->translate('tx_jobfinder_message.error_headline'),
                AbstractMessage::ERROR
            );
            $this->redirect('list');
        } else {
            $this->injectMetaInformation($job, $this->settings['siteName']);
        }
        //
        // Add some days to valid through date
        $daysLeft = (int)$this->settings['detail']['validThroughDate']['daysLeft'];
        $secondsLeft = $daysLeft * 60 * 60 * 24;
        $timestampLeft = time() + $secondsLeft;
        $validThroughDate = $job->getValidThroughDate();
        if ($validThroughDate instanceof DateTime) {
            $validThroughDateTimestamp = $job->getValidThroughDate()->getTimestamp();
            if ($timestampLeft > $validThroughDateTimestamp) {
                if (ExtensionManagementUtility::isLoaded('job_finder_pro')) {
                    /** @var \CodingMs\JobFinderPro\Service\JobFinderService $jobService */
                    $jobFinderService = GeneralUtility::makeInstance(
                        \CodingMs\JobFinderPro\Service\JobFinderService::class,
                        $this->uriBuilder
                    );
                    $jobFinderService->updateValidThroughDate(
                        $job,
                        (int)$this->settings['detail']['validThroughDate']['daysAdd']['min'],
                        (int)$this->settings['detail']['validThroughDate']['daysAdd']['max']
                    );
                    // Set publication date on current date
                    if ((boolean)$this->settings['detail']['validThroughDate']['refreshPublicationDate']) {
                        $job->setPublicationDate((new DateTime())->setTimestamp(time()));
                    }
                    //
                    // Ensure that everything is persisted
                    $this->jobRepository->update($job);
                    $this->persistenceManager->persistAll();
                }
            }
        }
        //
        // Suggested jobs
        $suggestedJobs = [];
        if (ExtensionManagementUtility::isLoaded('job_finder_pro')) {
            $suggestedJobs = $this->jobRepository->findSuggestedJobs($job, $this->settings['detail']['suggestedJobs']);
        }
        $this->view->assign('suggestedJobs', $suggestedJobs);
        //
        $this->view->assign('job', $job);
        $this->view->assign('content', $this->content);
        return $this->htmlResponse();
    }

    protected function injectMetaInformation(Job $job, string $siteName = '')
    {
        $title = $job->getHtmlTitle();
        if($title === '') {
            $title = $job->getTitle();
        }
        $description = $job->getMetaDescription();
        if($description === '') {
            $description = str_replace('&nbsp;', ' ', strip_tags($job->getDescription()));
        }
        /** @var MetaTagManagerRegistry $metaTagManager */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        //
        /** @var MetaTagManagerInterface $metaTagManagerDescription */
        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgTitle */
        $metaTagManagerOgTitle = $metaTagManagerRegistry->getManagerForProperty('og:title');
        $metaTagManagerOgTitle->addProperty('og:title', $title);
        //
        /** @var MetaTagManagerInterface $metaTagManagerAbstract */
        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', $job->getMetaAbstract());
        //
        /** @var MetaTagManagerInterface $metaTagManagerKeywords */
        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', $job->getMetaKeywords());
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgDescription */
        $metaTagManagerOgDescription = $metaTagManagerRegistry->getManagerForProperty('og:description');
        $metaTagManagerOgDescription->addProperty('og:description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgType */
        $metaTagManagerOgType = $metaTagManagerRegistry->getManagerForProperty('og:type');
        $metaTagManagerOgType->addProperty('og:type', 'WebPage');
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgSiteName */
        $metaTagManagerOgSiteName = $metaTagManagerRegistry->getManagerForProperty('og:site_name');
        $metaTagManagerOgSiteName->addProperty('og:site_name', $siteName);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgUrl */
        $metaTagManagerOgUrl = $metaTagManagerRegistry->getManagerForProperty('og:url');
        $metaTagManagerOgUrl->addProperty('og:url', GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        //
        // Preview image
        $previewImages = $job->getImages();
        if (count($previewImages) > 0) {
            /** @var FileReference $previewImage */
            $previewImage = $previewImages[0];
            /** @var ImageService $imageService */
            $imageService = GeneralUtility::makeInstance(ImageService::class);
            $image = $imageService->getImage($previewImage->getOriginalResource()->getLink(), $previewImage, true);
            //
            // Cropping
            if ($image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create((string)($cropString ?? ''));
            $cropVariant = 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);
            $processingInstructions = [
                'width' => 600,
                'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            //
            $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUrl = $imageService->getImageUri($processedImage, true);
            //
            /** @var MetaTagManagerInterface $metaTagManagerOgImage */
            $metaTagManagerOgImage = $metaTagManagerRegistry->getManagerForProperty('og:image');
            $metaTagManagerOgImage->addProperty('og:image', $imageUrl);
        }
        //
        /** @var PageTitleProvider $seoTitlePageTitleProvider*/
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($title);
    }

    /**
     * Display job teaser
     */
    public function teaserAction(): ResponseInterface
    {
        $limit = (int)$this->settings['teaser']['limit'];
        $sortBy = $this->settings['teaser']['sortBy'];
        $sortOrder = $this->settings['teaser']['sortOrder'];
        $jobs = $this->jobRepository->findAllTeaser($sortBy, $sortOrder, $limit);
        if ($jobs->count() === 0) {
            $this->addFlashMessage(
                $this->translate('tx_jobfinder_message.no_jobs_available'),
                $this->translate('tx_jobfinder_message.ok_headline'),
                AbstractMessage::INFO
            );
        }
        $this->view->assign('jobs', $jobs);
        $this->view->assign('content', $this->content);
        return $this->htmlResponse();
    }

    /**
     * Job application
     * @throws NoSuchArgumentException
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     */
    public function applicationAction(): ResponseInterface
    {
        //
        // Hide pro features
        if (!ExtensionManagementUtility::isLoaded('job_finder_pro')) {
            $this->addFlashMessage(
                $this->translate('tx_jobfinder_label.pro_version_required'),
                $this->translate('tx_jobfinder_message.error_headline'),
                AbstractMessage::ERROR
            );
            $this->redirect('list');
        }
        $job = null;
        if ($this->request->hasArgument('job')) {
            $jobUid = (int)$this->request->getArgument('job');
            $job = $this->jobRepository->findByIdentifier($jobUid);
        }
        if (!($job instanceof Job)) {
            // Job not found?!
            // Redirect to list view
            $this->addFlashMessage(
                $this->translate('tx_jobfinder_message.job_not_found', [$jobUid]),
                $this->translate('tx_jobfinder_message.error_headline'),
                AbstractMessage::ERROR
            );
            $this->redirect('list');
        }
        //
        $formValues = [];
        $formMessages = [];
        //
        // Validate gender
        if ($this->request->hasArgument('gender')) {
            $validGenders = ['male', 'female', 'other'];
            $formValues['gender'] = $this->request->getArgument('gender');
            if ($formValues['gender'] === '') {
                $formMessages['gender'][] = $this->translate('tx_jobfinder_message.error_application_gender_empty');
            } else if (!in_array($formValues['gender'], $validGenders)) {
                $formMessages['gender'][] = $this->translate('tx_jobfinder_message.error_application_gender_invalid');
            }
        }
        //
        // Validate firstname
        if ($this->request->hasArgument('firstname')) {
            $formValues['firstname'] = trim($this->request->getArgument('firstname'));
            if ($formValues['firstname'] === '') {
                $formMessages['firstname'][] = $this->translate('tx_jobfinder_message.error_application_firstname_empty');
            }
        }
        //
        // Validate lastname
        if ($this->request->hasArgument('lastname')) {
            $formValues['lastname'] = trim($this->request->getArgument('lastname'));
            if ($formValues['lastname'] === '') {
                $formMessages['lastname'][] = $this->translate('tx_jobfinder_message.error_application_lastname_empty');
            }
        }
        //
        // Validate mobile
        if ($this->request->hasArgument('mobile')) {
            $formValues['mobile'] = trim($this->request->getArgument('mobile'));
            if ($formValues['mobile'] === '') {
                $formMessages['mobile'][] = $this->translate('tx_jobfinder_message.error_application_mobile_empty');
            }
        }
        //
        // Validate email
        if ($this->request->hasArgument('email')) {
            $formValues['email'] = trim($this->request->getArgument('email'));
            if ($formValues['email'] === '') {
                $formMessages['email'][] = $this->translate('tx_jobfinder_message.error_application_email_empty');
            } else if (!ValidationUtility::isValidMail($formValues['email'])) {
                $formMessages['email'][] = $this->translate('tx_jobfinder_message.error_application_email_invalid');
            }
        }
        //
        // Validate message
        if ($this->request->hasArgument('message')) {
            $formValues['message'] = trim($this->request->getArgument('message'));
            if ($formValues['message'] === '') {
                $formMessages['message'][] = $this->translate('tx_jobfinder_message.error_application_message_empty');
            }
        }
        //
        // Validate terms
        if ($this->request->hasArgument('terms')) {
            $formValues['terms'] = trim($this->request->getArgument('terms'));
            if ($formValues['terms'] === '') {
                $formMessages['terms'][] = $this->translate('tx_jobfinder_message.error_application_terms_required');
            }
        }
        //
        // Validate files
        $files = [];
        $allowedFileTypes = $this->settings['application']['upload']['allowedFileTypes'];
        $maxFileSize = ((int)$this->settings['application']['upload']['maxFileSize'] * 1024 * 1024);
        for ($i = 1; $i <= 4; $i++) {
            $fileKey = 'file' . $i;
            //
            // Old v11 behaviour
            if((int)VersionNumberUtility::getCurrentTypo3Version() < 12) {
                if ($this->request->hasArgument($fileKey)) {
                    /** @var array $file */
                    $file = $this->request->getArgument($fileKey);
                    if (trim($file['name']) !== '') {
                        if (!FileUpload::checkExtension($file['name'], $allowedFileTypes)) {
                            $formMessages[$fileKey][] = $this->translate('tx_jobfinder_message.error_application_file_invalid_extension');
                        }
                        if ($file['size'] > $maxFileSize) {
                            $formMessages[$fileKey][] = $this->translate('tx_jobfinder_message.error_application_file_too_large');
                        }
                        if (empty($formMessages[$fileKey])) {
                            $files[] = [
                                'filename' => $file['name'],
                                'filepath' => $file['tmp_name'],
                                'filetype' => $file['type']
                            ];
                        }
                    }
                }
            } else {
                //
                // New v12 behaviour
                $uploadedFiles = $this->request->getUploadedFiles();
                if (isset($uploadedFiles[$fileKey])) {
                    /** @var UploadedFile $file */
                    $file = $uploadedFiles[$fileKey];
                    if (trim($file->getClientFilename()) !== '') {
                        if (!FileUpload::checkExtension($file->getClientFilename(), $allowedFileTypes)) {
                            $formMessages[$fileKey][] = $this->translate('tx_jobfinder_message.error_application_file_invalid_extension');
                        }
                        if ($file->getSize() > $maxFileSize) {
                            $formMessages[$fileKey][] = $this->translate('tx_jobfinder_message.error_application_file_too_large');
                        }
                        if (empty($formMessages[$fileKey])) {
                            $files[] = [
                                'filename' => $file->getClientFilename(),
                                'filepath' => $file->getTemporaryFileName(),
                                'filetype' => $file->getClientMediaType()
                            ];
                        }
                    }
                }
            }
        }
        //
        // Send application mail. Jobfinder pro extension needs to be installed
        if (!empty($formMessages)) {
            $this->addFlashMessage(
                $this->translate('tx_jobfinder_label.please_check_details'),
                $this->translate('tx_jobfinder_label.job_application_could_not_be_sent'),
                AbstractMessage::ERROR
            );
        } else if ($this->request->hasArgument('submit')) {
            if (!ExtensionManagementUtility::isLoaded('job_finder_pro')) {
                $this->addFlashMessage(
                    $this->translate('tx_jobfinder_label.pro_version_required'),
                    $this->translate('tx_jobfinder_label.job_application_could_not_be_sent'),
                    AbstractMessage::ERROR
                );
            }
            else {
                $jobLink = $this->uriBuilder->reset()
                    ->setCreateAbsoluteUri(true)
                    ->uriFor(
                        'show',
                        [
                            'job' => $job->getUid()
                        ],
                        'JobFinder'
                    );
                /** @var \CodingMs\JobFinderPro\Service\JobFinderService $jobService */
                $jobFinderService = GeneralUtility::makeInstance(
                    \CodingMs\JobFinderPro\Service\JobFinderService::class,
                    $this->uriBuilder
                );
                if ($jobFinderService->sendApplication(
                    $job,
                    $jobLink,
                    $this->settings,
                    $formValues,
                    $files,
                    $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK, 'JobFinder')
                )) {
                    $this->addFlashMessage(
                        $this->translate('tx_jobfinder_label.we_will_contact_you'),
                        $this->translate('tx_jobfinder_label.application_was_sent')
                    );
                    //$this->redirect('show', 'JobFinder', 'JobFinder', ['job' => $job->getUid()]);
                } else {
                    $this->addFlashMessage(
                        $this->translate('tx_jobfinder_label.contact_us'),
                        $this->translate('tx_jobfinder_label.job_application_could_not_be_sent'),
                        AbstractMessage::ERROR
                    );
                }
            }
        }
        //
        // Initialize fields, which are not set.
        if (!isset($formValues['gender'])) {
            $formValues['gender'] = 'male';
        }
        //
        // Prepare allowed file types for HTML upload
        $allowedFileTypes = GeneralUtility::trimExplode(
            ',',
            $this->settings['application']['upload']['allowedFileTypes'],
            true
        );
        $this->settings['application']['upload']['allowedFileTypesHtml'] = '.' . implode(',.', $allowedFileTypes);
        $this->view->assign('settings', $this->settings);
        //
        $this->view->assign('formValues', $formValues);
        $this->view->assign('formMessages', $formMessages);
        $this->view->assign('job', $job);
        $this->view->assign('content', $this->content);
        return $this->htmlResponse();
    }

    /**
     * Get a translation
     * @param $key
     * @param $arguments array
     * @return string|null
     */
    protected function translate($key, $arguments = [])
    {
        return LocalizationUtility::translate($key, 'JobFinder', $arguments);
    }

}
