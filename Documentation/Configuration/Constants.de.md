# TypoScript-Konstanten Einstellungen


## Job-Finder Container

| **Konstante**    | themes.configuration.container.jobs                                                                             |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Container für Job Datensätze                                                                                    |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | int+                                                                                                            |
| **Standardwert** | 0                                                                                                               |



## Job-Finder Seiten

| **Konstante**    | themes.configuration.pages.job.list                                                                             |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Job-Liste                                                                                                       |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | int+                                                                                                            |
| **Standardwert** | 0                                                                                                               |

| **Konstante**    | themes.configuration.pages.job.detail                                                                           |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Job-Details                                                                                                     |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | int+                                                                                                            |
| **Standardwert** | 0                                                                                                               |

| **Konstante**    | themes.configuration.pages.job.application                                                                      |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Bewerbung                                                                                                       |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | int+                                                                                                            |
| **Standardwert** | 0                                                                                                               |

| **Konstante**    | themes.configuration.javascript.google.maps.apiKey                                                              |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        |                                                                                                                 |
| **Beschreibung** |                                                                                                                 |
| **Typ**          |                                                                                                                 |
| **Standardwert** |                                                                                                                 |



## Job-Finder Templates

| **Konstante**    | themes.configuration.extension.job_finder.view.templateRootPath                                                 |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Template root path                                                                                              |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:job_finder/Resources/Private/Templates/                                                                     |

| **Konstante**    | themes.configuration.extension.job_finder.view.partialRootPath                                                  |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Partial root path                                                                                               |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:job_finder/Resources/Private/Partials/                                                                      |

| **Konstante**    | themes.configuration.extension.job_finder.view.layoutRootPath                                                   |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Layout root path                                                                                                |
| **Beschreibung** |                                                                                                                 |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:job_finder/Resources/Private/Layouts/                                                                       |



## Job PDF

| **Konstante**    | themes.configuration.extension.job_finder.pdf.job.fontPath                                                      |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Font-Pfad                                                                                                       |
| **Beschreibung** | Überschreibt den Pfad zu den PDF-Fonts                                                                          |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Fonts                                                                          |

| **Konstante**    | themes.configuration.extension.job_finder.pdf.job.imagePath                                                     |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Bildpfad                                                                                                        |
| **Beschreibung** | Überschreibt den Pfad zu den PDF-Bilder                                                                         |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Images                                                                         |

| **Konstante**    | themes.configuration.extension.job_finder.pdf.job.background                                                    |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Background-Pfad                                                                                                 |
| **Beschreibung** | Bspw. EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf oder fileadmin/Pdf/Expose/Briefpapier.pdf |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf                                                 |

| **Konstante**    | themes.configuration.extension.job_finder.pdf.job.partialRootPath                                               |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Partials-Pfad                                                                                                   |
| **Beschreibung** | Überschreibt den Pfad zu den Fluid-Partials                                                                     |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:jobfinder_pro/Resources/Private/Partials/                                                                   |

| **Konstante**    | themes.configuration.extension.job_finder.pdf.job.templateRootPath                                              |
|:-----------------|:----------------------------------------------------------------------------------------------------------------|
| **Label**        | Template-Pfad                                                                                                   |
| **Beschreibung** | Überschreibt den Pfad zu dem Expose-Template                                                                    |
| **Typ**          | string                                                                                                          |
| **Standardwert** | EXT:jobfinder_pro/Resources/Private/Templates/                                                                  |



