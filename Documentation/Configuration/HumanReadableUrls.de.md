# Sprechende URLs

## Slug Konfiguration (ab TYPO3 9.5)

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
    map:
      -job.pdf: 1702912991
  JobFinderPlugin:
    type: Extbase
    limitToPages:
      # Job list
      - 1849
      # Job detail/application view
      - 2023
    extension: JobFinder
    plugin: JobFinder
    routes:
      -
        routePath: '/{job_slug}'
        _controller: 'JobFinder::show'
        _arguments:
          job_slug: job
      -
        routePath: '/application/{job_slug}'
        _controller: 'JobFinder::application'
        _arguments:
          job_slug: job
    defaultController: 'JobFinder::list'
    aspects:
      job_slug:
        type: PersistedAliasMapper
        tableName: tx_jobfinder_domain_model_job
        routeFieldName: slug
        routeValuePrefix: /
  JobFinderProPlugin:
    type: Extbase
    limitToPages:
      - 225
    extension: JobFinderPro
    plugin: Pdf
    routes:
      -
        routePath: '/{job_slug}'
        _controller: 'Pdf::createJobPdf'
        _arguments:
          job_slug: job
    defaultController: 'Pdf::createJobPdf'
    aspects:
      job_slug:
        type: PersistedAliasMapper
        tableName: tx_jobfinder_domain_model_job
        routeFieldName: slug
        routeValuePrefix: /
```

## Realurl Konfiguration (bis TYPO3 9.5)

```php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    'domain.de' => [

        'fixedPostVars' => [
            'jobFinderConfiguration' => [
                0 => [
                    'GETvar' => 'tx_jobfinder_jobfinder[action]',
                    'valueMap' => [
                        'list' => '',
                        'show' => 'details',
                        'application' => 'application',
                    ],
                    'noMatch' => 'bypass',
                ],
                1 => [
                    'GETvar' => 'tx_jobfinder_jobfinder[controller]',
                    'valueMap' => [],
                    'noMatch' => 'bypass',
                ],
                2 => [
                    'GETvar' => 'tx_jobfinder_jobfinder[job]',
                    'lookUpTable' => [
                        'table' => 'tx_jobfinder_domain_model_job',
                        'id_field' => 'uid',
                        'alias_field' => 'CONCAT(title, \'-\', uid)',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                        'autoUpdate' => 1,
                        'expireDays' => 180,
                    ],
                ],
            ],
            51 => 'jobFinderConfiguration',
        ],

    ],
];
```
