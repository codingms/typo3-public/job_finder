# Sitemap.xml configuration for products

The following configuration enables you to generate a Sitemap.xml for your jobs:

```typo3_typoscript
plugin.tx_seo {
	config {
		xmlSitemap {
			sitemaps {
				jobFinder {
					provider = TYPO3\CMS\Seo\XmlSitemap\RecordsXmlSitemapDataProvider
					config {
						table = tx_jobfinder_domain_model_job
						additionalWhere = AND (tx_jobfinder_domain_model_job.hidden = 0)
						sortField = uid
						lastModifiedField = tstamp
						recursive = 1
						pid = 1956
						url {
							pageId = 1849
							fieldToParameterMap {
								uid = tx_jobfinder_jobfinder[job]
							}

							additionalGetParameters {
								tx_jobfinder_jobfinder.controller = JobFinder
								tx_jobfinder_jobfinder.action = show
							}
							useCacheHash = 1
						}
					}
				}
			}
		}
	}
}
```
