# TypoScript constant settings


## Job-Finder Container

| **Constant**      | themes.configuration.container.jobs                                                                                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Container for job records                                                                                           |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |



## Job-Finder Pages

| **Constant**      | themes.configuration.pages.job.list                                                                                 |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Job list                                                                                                            |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.pages.job.detail                                                                               |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Job details                                                                                                         |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.pages.job.application                                                                          |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Job application                                                                                                     |
| **Description**   |                                                                                                                     |
| **Type**          | int+                                                                                                                |
| **Default value** | 0                                                                                                                   |

| **Constant**      | themes.configuration.javascript.google.maps.apiKey                                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         |                                                                                                                     |
| **Description**   |                                                                                                                     |
| **Type**          |                                                                                                                     |
| **Default value** |                                                                                                                     |



## Job-Finder Templates

| **Constant**      | themes.configuration.extension.job_finder.view.templateRootPath                                                     |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Template root path                                                                                                  |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:job_finder/Resources/Private/Templates/                                                                         |

| **Constant**      | themes.configuration.extension.job_finder.view.partialRootPath                                                      |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Partial root path                                                                                                   |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:job_finder/Resources/Private/Partials/                                                                          |

| **Constant**      | themes.configuration.extension.job_finder.view.layoutRootPath                                                       |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Layout root path                                                                                                    |
| **Description**   |                                                                                                                     |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:job_finder/Resources/Private/Layouts/                                                                           |



## Job PDF

| **Constant**      | themes.configuration.extension.job_finder.pdf.job.fontPath                                                          |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Font path                                                                                                           |
| **Description**   | Overwrites the path to the PDF-Fonts                                                                                |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Fonts                                                                              |

| **Constant**      | themes.configuration.extension.job_finder.pdf.job.imagePath                                                         |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Image path                                                                                                          |
| **Description**   | Overwrites the path to the PDF-Images                                                                               |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Images                                                                             |

| **Constant**      | themes.configuration.extension.job_finder.pdf.job.background                                                        |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Background path                                                                                                     |
| **Description**   | For example EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf or fileadmin/Pdf/Expose/Briefpapier.pdf |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:fluid_fpdf/Resources/Private/Pdf/Normbriefbogen_DIN5008.pdf                                                     |

| **Constant**      | themes.configuration.extension.job_finder.pdf.job.partialRootPath                                                   |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Partials path                                                                                                       |
| **Description**   | Overwrites the path to the Fluid-Partials                                                                           |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:jobfinder_pro/Resources/Private/Partials/                                                                       |

| **Constant**      | themes.configuration.extension.job_finder.pdf.job.templateRootPath                                                  |
| :---------------- | :------------------------------------------------------------------------------------------------------------------ |
| **Label**         | Template path                                                                                                       |
| **Description**   | Overwrites the path to the Expose-Template                                                                          |
| **Type**          | string                                                                                                              |
| **Default value** | EXT:jobfinder_pro/Resources/Private/Templates/                                                                      |



