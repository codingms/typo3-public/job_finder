# Link-Handler verwenden

Um beispielsweise auf Jobs oder auch direkt auf Bewerbungs-Formulare zu verlinken, kann der entsprechende Link-Handler eingebunden werden.

Dafür muss die Wurzel-Seite der Webseite zur Bearbeitung geöffnet werden. Hier auf den Tab Resources wechseln und die TypoScript-Configuration `Job-Finder - LinkHandler application` und/oder `Job-Finder - LinkHandler job` einbinden.

Damit der Link-Handler seine Linkziele richtig verlinken kann, müssen zudem auch die TypoScript-Konstanten für die Pages gesetzt sein.
