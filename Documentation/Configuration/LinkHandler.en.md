# Using link handlers

Link handlers are used to create direct links to jobs or application forms.

Open the root page of the website in edit mode. Switch to the Resources tab and add in the TypoScript configuration `Job-Finder - LinkHandler application` and/or `Job-Finder - LinkHandler job`.

The TypoScript constants for the pages must also be set so that the link handlers link correctly.
